class InsuranceCompany {
  constructor() {
    this._name = 'Adewale\'s Insurance Company';
    this._availableRisks = [];
    this._soldPolicies = [];
  }

  // Get name of the company
  get name() {
    return this._name;
  }

  // Get list of the risks that can be insured.
  get availableRisks() {
    return this._availableRisks;
  }

  // Set list of the risks that can be insured. List can be updated at any time.
  setAvailableRisks(risks) {
    this._availableRisks = risks;
  }

  // Sell the policy.
  // nameOfInsuredObject: Name of the insured object. Must be unique in the given period.
  // validFrom: Date and time when policy starts. Can not be in the past
  // validMonths: (number) Policy period in full months
  // selectedRisks: List of risks objects that must be included in the policy
  sellPolicy(nameOfInsuredObject, validFrom, validMonths, selectedRisks) {
    if (
      this.policyIsSold(nameOfInsuredObject, validFrom, validMonths) 
    ) {
      throw new Error('Cannot create policies with same insured object name and same effective periods.');
    }

    return this.checkIfPolicyStartDateIsInPresent(nameOfInsuredObject, validFrom, validMonths, selectedRisks);
  }

  checkIfPolicyStartDateIsInPresent(nameOfInsuredObject, validFrom, validMonths, selectedRisks) {
    if (!moment().startOf('day').isAfter(validFrom)) {
      let policy = new Policy(nameOfInsuredObject, validFrom, validMonths, selectedRisks);

      this._soldPolicies.push(policy);
      return policy;
    }
    
    throw new Error('Date policy starts cannot be in the past.');
  }

  policyIsSold(nameOfInsuredObject, validFrom, validMonths) {
    return this._soldPolicies.find(soldPolicy => {
      return nameOfInsuredObject === soldPolicy.nameOfInsuredObject
      && moment(validFrom).format('YYYY-MM-DD') === soldPolicy.validFrom
      && validMonths === soldPolicy.validMonths;
    });
  }

  // Add risk to the policy of insured object.
  // nameOfInsuredObject: Name of insured object
  // risk: Risk that must be added
  // validFrom: Date when risk becomes active. Can not be in the past
  addRisk(nameOfInsuredObject, risk, validFrom) {
    if (!moment().startOf('day').isAfter(validFrom)) {
      let policy = this._soldPolicies.find(currentPolicy => {
        return nameOfInsuredObject === currentPolicy.nameOfInsuredObject;
      });

      this.checkIfRiskIsWithinPolicyPeriod(policy, risk, validFrom);
    } else {
      throw new Error('Date risk becomes active cannot be in the past.');
    }
  }

  checkIfRiskIsWithinPolicyPeriod(policy, risk, validFrom) {
    if (!moment(validFrom).isAfter(moment(policy.validTill))) {
      policy.addRisk(risk);
    } else {
      throw new Error('Date risk becomes active cannot exceed policy expiry.');
    }
  }

  // Gets policy with a risks at the given point of time.
  // nameOfInsuredObject: Name of insured object
  // effectiveDate: Point of date and time, when to get data about a policy
  getPolicy(nameOfInsuredObject, effectiveDate) {
    return this._soldPolicies.find(soldPolicy => {
      return nameOfInsuredObject === soldPolicy.nameOfInsuredObject
      && moment(effectiveDate).format('YYYY-MM-DD') === soldPolicy.validFrom
    });
  }
}
