class Policy {
  constructor(nameOfInsuredObject, validFrom, validMonths, selectedRisks) {
    this._nameOfInsuredObject = nameOfInsuredObject;
    this._validFrom = moment(validFrom).format('YYYY-MM-DD');
    this._validMonths = validMonths;
    this._selectedRisks = selectedRisks;
  }

  // Get name of insured object.
  get nameOfInsuredObject() {
    return this._nameOfInsuredObject;
  }

  // Get date when policy becomes active.
  get validFrom() {
    return this._validFrom;
  }

  get validMonths() {
    return this._validMonths;
  }

  // Get date when policy becomes inactive.
  get validTill() {
    return moment(this._validFrom).add(this._validMonths - 1, 'months').endOf('month').format('YYYY-MM-DD');
  }

  // Get total price of the policy. Calculate by summing up all insured risks.
  // Take into account that price of the risk is given for 1 full year. Policy/risk period can be shorter.
  get premium() {
    const initialValue = 0;
    return this._selectedRisks.reduce((accumulator, currentRisk) => accumulator + currentRisk.yearlyPrice * (this._validMonths / 12), initialValue);
  }

  // Get list of the Risks that are included in the policy at given time moment.
  get insuredRisks() {
    return this._selectedRisks;
  }

  addRisk(risk) {
    this._selectedRisks.push(risk);
  }
}
