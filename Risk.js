class Risk {
  constructor(name, yearlyPrice) {
    this._name = name;
    this._yearlyPrice = yearlyPrice;
  }
  
  // Get name of the Risk
  get name() {
    return this._name;
  }
  // Get yearly price of the Risk
  get yearlyPrice() {
    return this._yearlyPrice;
  }
}
